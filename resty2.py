#!flask/bin/python
from flask import Flask, request, jsonify
from flask_api import FlaskAPI
import pymongo
import datetime
import time
from datetime import datetime
import calendar
import os.path
import urllib

#route/var definitions
VERSION = "v"
PING_STATISTICS = "ps"
API_ROUTES = "ar"
API_SETTINGS = "as"
IP_LIST = "il"
DEVICE_SETTINGS = "ds"
PING_SETTINGS = "pse"
CHECK_DEVICE = "cd"
ERROR_REPORT = "er"
#values
VERSION_NUMBER = 1

#currently unused
TRACEROUTE_STATISTICS = "ts"
TRACEROUTE_SETTINGS = "tse"

def makeRoute(route):
	return "/" + route 

app = Flask(__name__)

@app.route('/')
def index():
    return "hello world"
	
@app.route(makeRoute(PING_STATISTICS), methods=['POST'])
def recievePingStatistics():
	#switch this to remote server later
	mongo_uri = "mongodb+srv://" + "ligma" + ":" + "bowls" + "@superbowl-test-afu2e.mongodb.net/test?retryWrites=true"
	myclient = pymongo.MongoClient(mongo_uri)
	buffer = request.get_json()
	saveMe = {"timestamp":buffer['ts'],
		"mac address":buffer['mac'],
		"client ip":request.remote_addr,
		"target ip":buffer['ip'],
		"requests sent":buffer['tx'],
		"responses received":buffer['rx'],
		"minimum ping time":buffer['min'],
		"maximum ping time":buffer['max'],
		"average ping time":buffer['avg'],
		"time received":calendar.timegm(datetime.utcnow().utctimetuple())}
	mydb = myclient["superb-owl"]
	mycol = mydb["ping"]
	mycol.insert_one(saveMe)
	response = app.response_class(status=201,mimetype='application/json')
	return response

@app.route(makeRoute(API_SETTINGS), methods=['GET'])
def getAPIRoutes():
	response = jsonify({
		VERSION:VERSION_NUMBER,
		PING_STATISTICS:makeRoute(PING_STATISTICS),
		TRACEROUTE_SETTINGS:makeRoute(TRACEROUTE_SETTINGS),
		API_ROUTES:makeRoute(API_ROUTES),
		API_SETTINGS:makeRoute(API_SETTINGS),
		DEVICE_SETTINGS:makeRoute(DEVICE_SETTINGS),
		IP_LIST:makeRoute(IP_LIST),
		PING_SETTINGS:makeRoute(PING_SETTINGS),
		TRACEROUTE_SETTINGS:makeRoute(TRACEROUTE_SETTINGS)})
	return response
	
@app.route(makeRoute(API_SETTINGS), methods=['GET'])
def getAPISettings():
	response = jsonify({"v":1,"aad":"idk lol","apo":5000,"ake":420})
	return response

@app.route(makeRoute(IP_LIST), methods=['GET'])
def getIPList():
	response = jsonify({"v":1,"ip":[8,8,8,8,69,63,176,13]})
	return response

@app.route(makeRoute(DEVICE_SETTINGS), methods=['GET'])
def getDeviceSettings():
	response = jsonify({"v":1,"mac":[222, 173, 190, 239, 254, 237]})
	return response

@app.route(makeRoute(PING_SETTINGS), methods=['GET'])
def getPingSettings():
	response = jsonify({"v":1,"pm":64,"pr":3,"pn":3})
	return response

@app.route(makeRoute(CHECK_DEVICE), methods=['GET'])
def checkDevice():
	response = jsonify({"m":600,"s":0,"u":0,"t":time.time()})
	return response

@app.route(makeRoute(ERROR_REPORT), methods=['POST'])
def errorReport():
	myclient = pymongo.MongoClient("mongodb://localhost:27017/")
	buffer = request.get_json()
	saveMe = {"net down count":buffer['nnn'],
		"LAN down count":buffer['nnl'],
		"server down count":buffer['nsd'],
		"server error count":buffer['nse'],
		"device restart count":buffer['nr'],
		"device error count":buffer['nr'],
		"original server timestamp":buffer['ots'],
		"arduino timestamp":buffer['ts'],
		"time until sync":buffer['tl'],
		"number of ping results":buffer['npr'],
		"ping results":['pr']} #trim down ping results
	mydb = myclient["superb-owl"]
	mycol = mydb["error"]
	mycol.insert_one(saveMe)
	response = app.response_class(status=201,mimetype='application/json')
	return response

# RS_NONE, // 0
# RS_DEVICE_SETTINGS, // 1
# RS_API_SETTINGS, // 2
# RS_API_ROUTES, // 3
# RS_PING_SETTINGS, // 4
# RS_TRACEROUTE_SETTINGS, // 5
# RS_IP_LIST, // 6
# RS_RESET // 7

if __name__ == '__main__':
    app.run(debug=True)