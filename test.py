from twisted.internet import protocol, reactor, endpoints
import json

class Echo(protocol.Protocol):
    def dataReceived(self, data):
		print(data)
        self.transport.write("cool and good"	)

class EchoFactory(protocol.Factory):
    def buildProtocol(self, addr):
        return Echo()

class Config(protocol.Protocol):
    def dataReceived(self, data):
		print(data)
        #self.transport.write(data)
    def connectionMade(self):
        print("WOW")

class ConfigFactory(protocol.Factory):
    def buildProtocol(self, addr):
        return Config()

endpoints.serverFromString(reactor, "tcp:15000").listen(ConfigFactory())
endpoints.serverFromString(reactor, "tcp:15001").listen(EchoFactory())
reactor.run()