#!flask/bin/python
from flask import Flask, request, jsonify
from flask_api import FlaskAPI
import sqlite3
import datetime
import time
from datetime import datetime
import calendar
import os.path


#route/var definitions
VERSION = "v"
PING_STATISTICS = "ps"
API_ROUTES = "ar"
API_SETTINGS = "as"
IP_LIST = "il"
DEVICE_SETTINGS = "ds"
PING_SETTINGS = "pse"
CHECK_DEVICE = "cd"
ERROR_REPORT = "er"
#values
VERSION_NUMBER = 1

#currently unused
TRACEROUTE_STATISTICS = "ts"
TRACEROUTE_SETTINGS = "tse"

def makeRoute(route):
	return "/" + route 

def makeDB():
	conn = sqlite3.connect('temp_database.sqlite')
	c = conn.cursor()
	c.execute("CREATE TABLE `ErrorReports` (`id` UUID PRIMARY KEY, `mac_addr` TEXT NOT NULL DEFAULT NULL, `num_no_net` INTEGER DEFAULT NULL, `num_no_lan` INTEGER DEFAULT NULL, `num_restarted` INTEGER DEFAULT NULL, `num_server_down` INTEGER DEFAULT NULL, `num_server_error` INTEGER DEFAULT NULL, `num_device_error` INTEGER DEFAULT NULL, `timestamp` DATETIME DEFAULT NULL, `original_timestamp` DATETIME DEFAULT NULL, `time_left` INTEGER DEFAULT NULL, `num_ping_results` INTEGER DEFAULT 0, `created_at` DATETIME NOT NULL, `updated_at` DATETIME NOT NULL);")
	c.execute("CREATE TABLE PingStatistics (id UUID PRIMARY KEY, timestamp DATETIME DEFAULT NULL, mac_addr TEXT NOT NULL DEFAULT NULL, device_ip_address TEXT DEFAULT NULL, ping_ip_address TEXT DEFAULT NULL, num_packets_transmitted INTEGER DEFAULT NULL, num_packets_received INTEGER DEFAULT NULL, packet_loss DECIMAL DEFAULT NULL, min_ping INTEGER DEFAULT NULL, max_ping INTEGER DEFAULT NULL, avg_ping INTEGER DEFAULT NULL, created_at DATETIME NOT NULL);")
	c.execute("CREATE TABLE `TracerouteStatistics` (`id` UUID PRIMARY KEY, `ver` INTEGER DEFAULT NULL, `mac` VARCHAR(255) DEFAULT NULL, `entry_id` UUID DEFAULT NULL, `seq` INTEGER DEFAULT NULL, `data` VARCHAR(255) DEFAULT NULL, `if_deleted` TINYINT(1) NOT NULL DEFAULT 0, `created_at` DATETIME NOT NULL, `updated_at` DATETIME NOT NULL);")
	conn.commit()
	conn.close()

app = Flask(__name__)

@app.route('/')
def index():
    return "hello world"
	
@app.route(makeRoute(PING_STATISTICS), methods=['POST'])
def recievePingStatistics():
	content = request.get_json()
	if os.path.isfile('temp_database.sqlite') == False:
		makeDB()
	conn = sqlite3.connect('temp_database.sqlite')
	buffer = request.get_json()
	c = conn.cursor()
	cmd = "INSERT INTO `PingStatistics` (timestamp, mac_addr, device_ip_address, ping_ip_address, num_packets_transmitted, num_packets_received, min_ping, max_ping, avg_ping,created_at) VALUES (?,?,?,?,?,?,?,?,?,?)"
	c.execute(cmd,(buffer['ts'],buffer['mac'],request.remote_addr,buffer['ip'],buffer['tx'],buffer['rx'],buffer['min'],buffer['max'],buffer['avg'],calendar.timegm(datetime.utcnow().utctimetuple())))
	conn.commit()
	conn.close()
	response = app.response_class(status=201,mimetype='application/json')
	return response

@app.route(makeRoute(API_SETTINGS), methods=['GET'])
def getAPIRoutes():
	response = jsonify({
		VERSION:VERSION_NUMBER,
		PING_STATISTICS:makeRoute(PING_STATISTICS),
		TRACEROUTE_SETTINGS:makeRoute(TRACEROUTE_SETTINGS),
		API_ROUTES:makeRoute(API_ROUTES),
		API_SETTINGS:makeRoute(API_SETTINGS),
		DEVICE_SETTINGS:makeRoute(DEVICE_SETTINGS),
		IP_LIST:makeRoute(IP_LIST),
		PING_SETTINGS:makeRoute(PING_SETTINGS),
		TRACEROUTE_SETTINGS:makeRoute(TRACEROUTE_SETTINGS)})
	return response
	
@app.route(makeRoute(API_SETTINGS), methods=['GET'])
def getAPISettings():
	response = jsonify({"v":1,"aad":"idk lol","apo":5000,"ake":420})
	return response

@app.route(makeRoute(IP_LIST), methods=['GET'])
def getIPList():
	response = jsonify({"v":1,"ip":[8,8,8,8,69,63,176,13]})
	return response

@app.route(makeRoute(DEVICE_SETTINGS), methods=['GET'])
def getDeviceSettings():
	response = jsonify({"v":1,"mac":[222, 173, 190, 239, 254, 237]})
	return response

@app.route(makeRoute(PING_SETTINGS), methods=['GET'])
def getPingSettings():
	response = jsonify({"v":1,"pm":64,"pr":3,"pn":3})
	return response

@app.route(makeRoute(CHECK_DEVICE), methods=['GET'])
def checkDevice():
	response = jsonify({"m":600,"s":0,"u":0,"t":time.time()})
	return response

@app.route(makeRoute(ERROR_REPORT), methods=['GET'])
def errorReport():
	#fix this
	response = jsonify({"m":600,"s":0,"u":0,"t":time.time()})
	return response
	

# RS_NONE, // 0
# RS_DEVICE_SETTINGS, // 1
# RS_API_SETTINGS, // 2
# RS_API_ROUTES, // 3
# RS_PING_SETTINGS, // 4
# RS_TRACEROUTE_SETTINGS, // 5
# RS_IP_LIST, // 6
# RS_RESET // 7

if __name__ == '__main__':
    app.run(debug=True)